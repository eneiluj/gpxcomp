#! /usr/bin/env python
#-*- coding: utf-8 -*-

import sys, os, time, math, codecs
import gpxpy, gpxpy.gpx
#import inspect
from PyQt5.QtWidgets import QApplication
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import  *
from PyQt5.QtGui import *
from PyQt5 import uic
from PyQt5 import QtCore, QtGui, QtWebKit, QtNetwork, QtWidgets, QtWebKitWidgets
from PyQt5.QtCore import QFile, QCoreApplication
from PyQt5.QtWidgets import QApplication, QVBoxLayout, QHBoxLayout, QLabel, QTableWidgetItem
from PyQt5.QtWidgets import QSizePolicy, QFileDialog, QStyle, QStyleFactory, QMessageBox, QSpacerItem
import functools
import comparison

versionFilePath = '%s/version.txt'%os.path.dirname(os.path.realpath(sys.argv[0]))
if os.path.exists(versionFilePath):
    f = open(versionFilePath, 'r')
    VERSION = f.readline().strip()
    VERSION_DATE = f.readline().strip()
    f.close()

formGpxcomp,  baseGpxcomp = uic.loadUiType('%s/uis/gpxcomp.ui'%os.path.dirname(os.path.realpath(sys.argv[0])))

class Gpxcomp(formGpxcomp, baseGpxcomp):
    def __init__(self, app, gpx1path='', gpx2path='', parent=None):
        super(Gpxcomp, self).__init__(parent)
        self.app = app
        self.gpx1path = gpx1path
        self.gpx2path = gpx2path
        self.name1 = None
        self.name2 = None
        self.taggedGeo12 = None
        self.taggedGeo21 = None

        self.providerToUrl = {
                'OpenStreetMap' : 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                'OpenCycleMap' : 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
                'OpenTopoMap' : 'http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
                'ESRI WorldImagery' : 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
        }
        self.providerToAttrib = {
                'OpenStreetMap' : '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                'OpenCycleMap' : '&copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                'OpenTopoMap' : 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
                'ESRI WorldImagery' : 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
        }

        self.createWidgets()
        self.setWindowTitle("GpxComp")

    def createWidgets(self):
        self.ui = self
        self.ui.setupUi(self)

        self.ui.toolBar.hide()
        self.ui.statusBar.hide()

        file_menu = self.ui.menubar.addMenu("&File")
        self.file_menu = file_menu
        action = file_menu.addAction(self.style().standardIcon(QStyle.SP_DirOpenIcon),"Load first &Gpx file",self.loadGpx1,QKeySequence(Qt.CTRL + Qt.Key_O))
        action = file_menu.addAction(self.style().standardIcon(QStyle.SP_DirOpenIcon),"Load second &Gpx file",self.loadGpx2,QKeySequence(Qt.CTRL + Qt.Key_I))
        action = file_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarCloseButton),"&Quit",self.close,QKeySequence(Qt.CTRL + Qt.Key_Q))

        map_menu = self.ui.menubar.addMenu("&Map")
        map_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarMinButton),"Zoom out",self.zoomOut,QKeySequence(Qt.CTRL + Qt.Key_Minus))
        map_menu.addAction(self.style().standardIcon(QStyle.SP_TitleBarMaxButton),"Zoom in",self.zoomIn,QKeySequence(Qt.CTRL + Qt.Key_Plus))

        interf_menu = self.ui.menubar.addMenu("&Interface")
        interf_menu.addAction(self.style().standardIcon(QStyle.SP_ArrowLeft),"Move splitter left",self.moveSplitterLeft,QKeySequence(Qt.CTRL + Qt.Key_PageUp))
        interf_menu.addAction(self.style().standardIcon(QStyle.SP_ArrowRight),"Move splitter right",self.moveSplitterRight,QKeySequence(Qt.CTRL + Qt.Key_PageDown))

        # about window
        self.aboutWindow = uic.loadUi("%s/uis/about.ui"%os.path.dirname(os.path.realpath(sys.argv[0])))
        self.aboutWindow.parent = self
        self.aboutWindow.setWindowTitle('About GpxComp')
        ui = self.aboutWindow
        ui.logoLabel.setPixmap(QPixmap("%s/js/images/gpxcomp.png"%os.path.dirname(os.path.realpath(sys.argv[0]))))
        txt = str(self.aboutWindow.infoLabel.text())
        txt = txt.replace('vvv',VERSION).replace('ddd',VERSION_DATE)
        self.aboutWindow.infoLabel.setText(txt)
        ui.okButton.clicked.connect(self.aboutWindow.close)

        help_menu = self.ui.menubar.addMenu("&Help")
        help_menu.addAction("&About",self.aboutWindow.show)

        # buttons

        self.ui.browseGpx1Button.clicked.connect(self.loadGpx1)
        self.ui.browseGpx1Button.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))
        self.ui.browseGpx2Button.clicked.connect(self.loadGpx2)
        self.ui.browseGpx2Button.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))

        # layer combo

        for p in self.providerToUrl:
            self.ui.layerCombo.addItem(p)
        self.ui.layerCombo.currentIndexChanged.connect(self.mapProviderChanged)

        # map

        view = self.view = QtWebKitWidgets.QWebView(self)
        sp = QSizePolicy()
        sp.setVerticalPolicy(QSizePolicy.Expanding)
        sp.setHorizontalPolicy(QSizePolicy.Expanding)
        view.setSizePolicy(sp)

        cache = QtNetwork.QNetworkDiskCache()
        cache.setCacheDirectory(os.path.expanduser('~/.gpxcomp/cache'))
        view.page().networkAccessManager().setCache(cache)
        view.page().networkAccessManager()

        view.page().mainFrame().addToJavaScriptWindowObject("MainWindow", self)
        view.page().setLinkDelegationPolicy(QtWebKitWidgets.QWebPage.DelegateAllLinks)
        view.load(QtCore.QUrl('file://%s/js/map.html'%os.path.dirname(os.path.realpath(sys.argv[0]))))
        view.loadFinished.connect(self.onLoadFinished)
        view.linkClicked.connect(QtGui.QDesktopServices.openUrl)

        self.ui.verticalLayout.insertWidget(2, view)

        # give enough space to table
        self.ui.tableFrame.resize(400, 50)

        self.ui.criteriaCombo.currentIndexChanged.connect(self.drawTracks)

        # final actions
        self.ui.latlngEdit.setFocus()

        self.ui.statTable.verticalHeader().resizeSection(7, 50)
        self.ui.statTable.verticalHeader().resizeSection(8, 50)

    def moveSplitterLeft(self):
        sizes = self.ui.splitter.sizes()
        sizes[0] -= 50
        sizes[1] += 50
        self.ui.splitter.setSizes(sizes)

    def moveSplitterRight(self):
        sizes = self.ui.splitter.sizes()
        sizes[0] += 50
        sizes[1] -= 50
        self.ui.splitter.setSizes(sizes)

    def zoomOut(self):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('zoomOut();')

    def zoomIn(self):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('zoomIn();')

    def mapProviderChanged(self):
        sel = str(self.ui.layerCombo.currentText())
        js = "changeTileLayer('%s', '%s');"%(self.providerToUrl[sel], self.providerToAttrib[sel])
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript("%s"%js)

    def loadGpx1(self, checked=False, filePath=''):
        if filePath == '':
            qfd = QFileDialog()
            filePath = str(qfd.getOpenFileName(parent=self, caption="Choose a GPX file", filter='Gpx file (*.gpx);; All files (*)')[0])

        if os.path.exists(filePath):
            self.ui.gpx1PathEdit.setText(filePath)
            gpx2path = self.ui.gpx2PathEdit.text()
            if os.path.exists(gpx2path):
                self.compare()

    def loadGpx2(self, checked=False, filePath=''):
        if filePath == '':
            qfd = QFileDialog()
            filePath = str(qfd.getOpenFileName(parent=self, caption="Choose a GPX file", filter='Gpx file (*.gpx);; All files (*)')[0])

        if os.path.exists(filePath):
            self.ui.gpx2PathEdit.setText(filePath)
            gpx1path = self.ui.gpx1PathEdit.text()
            if os.path.exists(gpx1path):
                self.compare()

    def compare(self):
        self.gpx1path = str(self.ui.gpx1PathEdit.text())
        self.gpx2path = str(self.ui.gpx2PathEdit.text())
        self.name1 = os.path.basename(self.gpx1path)
        self.name2 = os.path.basename(self.gpx2path)
        # produce geojsons

        f1 = codecs.open(self.gpx1path, 'r', 'utf-8')
        content1 = f1.read()
        f1.close()
        f2 = codecs.open(self.gpx2path, 'r', 'utf-8')
        content2 = f2.read()
        f2.close()
        comp = comparison.compareTwoGpx(content1, self.name1, content2, self.name2)
        index12 = comp[0]
        index21 = comp[1]

        self.taggedGeo12 = str(comparison.gpxTracksToGeojson(content1, self.name1, index12))
        self.taggedGeo21 = str(comparison.gpxTracksToGeojson(content2, self.name2, index21))

        self.putGeneralStats()

        # load geojsons in map
        self.drawTracks()

    def drawTracks(self):
        criteria = self.ui.criteriaCombo.currentText()

        if self.taggedGeo12 and self.taggedGeo21:
            frame = self.view.page().mainFrame()
            frame.evaluateJavaScript("drawResults('%s', '%s', '%s', '%s', '%s');"%(criteria, self.name1, self.name2, self.taggedGeo12, self.taggedGeo21))

    def putGeneralStats(self):
        gpx1path = str(self.ui.gpx1PathEdit.text())
        gpx2path = str(self.ui.gpx2PathEdit.text())
        name1 = os.path.basename(gpx1path)
        name2 = os.path.basename(gpx2path)

        self.ui.statTable.setHorizontalHeaderItem(0,QTableWidgetItem(name1))
        self.ui.statTable.setHorizontalHeaderItem(1,QTableWidgetItem(name2))

        for elem in [(gpx1path, 0), (gpx2path, 1)]:
            gpxo = gpxpy.parse(open(elem[0]))
            l2d = '{:.3f}'.format(gpxo.length_2d() / 1000.)
            l3d = '{:.3f}'.format(gpxo.length_3d() / 1000.)

            moving_time, stopped_time, moving_distance, stopped_distance, max_speed = gpxo.get_moving_data()
            movtime = comparison.format_time(moving_time)
            pausetime = comparison.format_time(stopped_time)
            maxsp = '{:.2f}'.format(max_speed * 60. ** 2 / 1000. if max_speed else 0)

            uphill, downhill = gpxo.get_uphill_downhill()
            uph = '{:.2f}'.format(uphill)
            dwh = '{:.2f}'.format(downhill)

            start_time, end_time = gpxo.get_time_bounds()

            points_no = str(len(list(gpxo.walk(only_points=True))))
            
            self.ui.statTable.setItem(0,elem[1], QTableWidgetItem(l2d))
            self.ui.statTable.setItem(1,elem[1], QTableWidgetItem(l3d))
            self.ui.statTable.setItem(2,elem[1], QTableWidgetItem(movtime))
            self.ui.statTable.setItem(3,elem[1], QTableWidgetItem(pausetime))
            self.ui.statTable.setItem(4,elem[1], QTableWidgetItem(maxsp))
            self.ui.statTable.setItem(5,elem[1], QTableWidgetItem(uph))
            self.ui.statTable.setItem(6,elem[1], QTableWidgetItem(dwh))
            self.ui.statTable.setItem(7,elem[1], QTableWidgetItem(str(start_time)))
            self.ui.statTable.setItem(8,elem[1], QTableWidgetItem(str(end_time)))
            self.ui.statTable.setItem(9,elem[1], QTableWidgetItem(points_no))

    def timeFmt(self, sec):
        if sec >= 3600:
            return '%s:%.2d:%.2d'%(sec/3600, (sec%3600)/60, sec%60)
        else:
            return '%.2d:%.2d'%(sec/60, sec%60)

    def putMarkerOn(self, lat, lng, follow):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('putMarkerOn(%s, %s, %s);'%(lat, lng, str(follow).lower()))

    def onLoadFinished(self):
        with open('%s/js/map.js'%os.path.dirname(os.path.realpath(sys.argv[0])), 'r') as f:
            frame = self.view.page().mainFrame()
            frame.evaluateJavaScript(f.read())
        # play files given by CLI
        if self.gpx1path and self.gpx2path:
            self.loadGpx1(filePath=self.gpx1path)
            self.loadGpx2(filePath=self.gpx2path)

    @QtCore.pyqtSlot(float, float)
    def onMapMove(self, lat, lng):
        self.ui.latlngEdit.setText('Lng: {:.5f}, Lat: {:.5f}'.format(lng, lat))

    def panMap(self, lng, lat):
        frame = self.view.page().mainFrame()
        frame.evaluateJavaScript('map.panTo(L.latLng({}, {}));'.format(lat, lng))

if __name__ == '__main__':
    app = QApplication(sys.argv)

    # create .gpxcomp
    if not os.path.exists(os.path.expanduser('~/.gpxcomp')):
        os.mkdir(os.path.expanduser('~/.gpxcomp'))

    # style
    if not sys.platform.startswith('darwin'):
        if "Breeze" in QStyleFactory.keys():
            app.setStyle("Breeze")
        else:
            app.setStyle("Fusion")

    # CLI args
    if len(sys.argv) > 2 and os.path.exists(sys.argv[1]) and os.path.exists(sys.argv[2]):
        myapp = Gpxcomp(app, os.path.realpath(sys.argv[1]), os.path.realpath(sys.argv[2]))
    else:
        myapp = Gpxcomp(app)
    myapp.show()
    sys.exit(app.exec_())
