var map = L.map('map').setView([0, 0], 2);
L.control.scale({metric: true, imperial: true, position:'topleft'}).addTo(map);

var tilelayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
})

tilelayer.addTo(map);

function changeTileLayer(url, attrib){
    if (tilelayer !== null){
        map.removeLayer(tilelayer);
    }

    tilelayer = L.tileLayer(url, {
            maxZoom: 18,
            attribution: attrib
    });
    tilelayer.addTo(map);

}

if(typeof MainWindow != 'undefined') {
    var onMapMove = function() { MainWindow.onMapMove(map.getCenter().lat, map.getCenter().lng) };
    map.on('move', onMapMove);
    onMapMove();
}

function zoomIn(){
    map.zoomIn();
}

function zoomOut(){
    map.zoomOut();
}

var gpxlayers = [null, null];

// if criteria or track pair is changed on the page, dynamically draw
// considering changes
function drawResults(criteria, name1, name2, data1, data2){
    for(n=0;n<2;n++){
        if (gpxlayers[n] !== null){
            map.removeLayer(gpxlayers[n]);
        }
    }

    //var name1 = $( 'select option:selected' ).attr('name1');
    //var name2 = $( 'select option:selected' ).attr('name2');
    var cleaname1 = name1.replace('.gpx','').replace('.GPX','').replace(' ','_');
    var cleaname2 = name2.replace('.gpx','').replace('.GPX','').replace(' ','_');
    //var data1 = $('#'+cleaname1+cleaname2).html();
    //var data2 = $('#'+cleaname2+cleaname1).html();
    var odata1 = $.parseJSON(data1);
    var odata2 = $.parseJSON(data2);

    var results = [odata1, odata2];
    var names = [name1, name2];
    var n;
    for(n=0;n<2;n++){
        //delete gpxvcomp.layers[n]
        gpxlayers[n] = new L.geoJson(results[n], {
            weight: 4,
            style: function (feature) {
                return {color: getColor(names[n], names[1-n], criteria,
                        feature.properties), opacity: 0.9};
            },
            onEachFeature: function (feature, layer) {
                var linecolor = getColor(names[n], names[1-n], criteria, feature.properties);
                var tooltiptxt;
                if (linecolor === 'blue'){
                    tooltiptxt = names[n];
                }
                else if (linecolor === 'green'){
                    tooltiptxt = names[n]+'<br/>(better in '+criteria+')';
                }
                else if (linecolor === 'red'){
                    tooltiptxt = names[n]+'<br/>(worse in '+criteria+')';
                }
                layer.bindTooltip(tooltiptxt, {sticky: true});

                var txt ='';
                txt = txt + '<h3 style="text-align:center;">Track : '+
                            names[n]+'</h3><hr/>';
                if(feature.properties.time !== null)
                {
                    txt = txt + '<div style="width:100%;text-align:center;">'+
                                '<b><u>Divergence details</u></b></div>';

                    var shorter = (('shorterThan' in feature.properties) &&
                        (feature.properties.shorterThan.length > 0)
                    );
                    var distColor = shorter ? 'green' : 'red';

                    txt = txt + '<ul><li style="color:'+distColor+
                        ';"><b>Divergence distance</b>&nbsp;: '+
                        parseFloat(feature.properties.distance).toFixed(2)+
                        ' &nbsp;m</li>';
                    if (shorter){
                        txt = txt +'<li style="color:green">is shorter than '+
                              '&nbsp;: <div style="color:red">';
                        for(var y=0; y<feature.properties.shorterThan.length; y++){
                            var other=feature.properties.shorterThan[y];
                            txt = txt +other+' ('+
                            parseFloat(feature.properties.distanceOthers[other]).toFixed(2)+' m)';
                        }
                        txt = txt + '</div> &nbsp;</li>';
                    }
                    else{
                        txt = txt +'<li style="color:red">is longer than '+
                              '&nbsp;: <div style="color:green">';
                        for(var y=0; y<feature.properties.longerThan.length; y++){
                            var other=feature.properties.longerThan[y];
                            txt = txt+other+' ('+
                            parseFloat(feature.properties.distanceOthers[other]).toFixed(2)+' m)';
                        }
                        txt = txt + '</div> &nbsp;</li>';
                    }

                    var quicker = (('quickerThan' in feature.properties) &&
                        (feature.properties.quickerThan.length > 0)
                    );
                    var timeColor = quicker ? 'green' : 'red';

                    txt = txt +'<li style="color:'+timeColor+';"><b>'+
                        'Divergence time</b>&nbsp;: '+
                        feature.properties.time+' &nbsp;</li>';
                    if (quicker){
                        txt = txt +'<li style="color:green">'+
                            'is quicker than '+
                              '&nbsp;: <div style="color:red">';
                        for(var y=0; y<feature.properties.quickerThan.length; y++){
                            var other=feature.properties.quickerThan[y];
                            txt = txt+other+' ('+feature.properties.timeOthers[other]+')';
                        }
                        txt = txt + '</div> &nbsp;</li>';
                    }
                    else{
                        txt = txt +'<li style="color:red">is slower than '+
                              '&nbsp;: <div style="color:green">';
                        for(var y=0; y<feature.properties.slowerThan.length; y++){
                            var other=feature.properties.slowerThan[y];
                            txt = txt+other+' ('+feature.properties.timeOthers[other]+')';
                        }
                        txt = txt + '</div> &nbsp;</li>';
                    }

                    var lessDeniv = (('lessPositiveDenivThan' in feature.properties) &&
                        (feature.properties.lessPositiveDenivThan.length > 0)
                    );
                    var denivColor = lessDeniv ? 'green' : 'red';

                    txt = txt + '<li style="color:'+denivColor+';"><b>'+
                        'Cumulative elevation gain </b>'+
                        '&nbsp;: '+
                        parseFloat(feature.properties.positiveDeniv).toFixed(2)+
                        ' &nbsp;m</li>';
                    if (lessDeniv){
                        txt = txt +'<li style="color:green">is less than '+
                              '&nbsp;: <div style="color:red">';
                        for(var y=0; y<feature.properties.lessPositiveDenivThan.length; y++){
                            var other=feature.properties.lessPositiveDenivThan[y];
                            txt = txt+other+' ('+
                            parseFloat(feature.properties.positiveDenivOthers[other]).toFixed(2)+' m)';
                        }
                        txt = txt + '</div> &nbsp;</li>';
                    }
                    else{
                        txt = txt +'<li style="color:red">is more than '+
                              '&nbsp;: <div style="color:green">';
                        for(var y=0; y<feature.properties.morePositiveDenivThan.length; y++){
                            var other=feature.properties.morePositiveDenivThan[y];
                            txt = txt+other+' ('+
                            parseFloat(feature.properties.positiveDenivOthers[other]).toFixed(2)+' m)';
                        }
                        txt = txt + '</div> &nbsp;</li>';
                    }
                    txt = txt + '</ul>';
                }
                else{
                    txt = txt + '<li><b>There is no divergence here</b></li>';
                }
                txt = txt + '<hr/>';
                txt = txt + '<div style="text-align:center">';
                txt = txt + '<b><u>Segment details</u></b></div>';
                txt = txt + '<ul>';
                txt = txt + '<li>Segment id : '+feature.properties.id+'</li>';
                txt = txt + '<li>From : '+feature.geometry.coordinates[0][1]+
                      ' ; '+feature.geometry.coordinates[0][0]+'</li>';
                var lastCoordIndex = feature.geometry.coordinates.length-1;
                txt = txt + '<li>To : '+feature.geometry.coordinates[lastCoordIndex][1]+
                      ' ; '+feature.geometry.coordinates[lastCoordIndex][0]+'</li>';
                try{
                    var tsplt = feature.properties.timestamps.split(' ; ');
                    var t1s = tsplt[0];
                    var t2s = tsplt[1];
                }
                catch(err){
                    var t1s = 'no date';
                    var t2s = 'no date';
                }
                txt = txt + '<li>Time :<br/>&emsp;'+t1s+' &#x21e8; <br/>&emsp;'+t2s+'</li>';
                txt = txt + '<li>Elevation : '+feature.properties.elevation[0]+
                      ' &#x21e8; '+feature.properties.elevation[1]+'m</li>';
                txt = txt + '</ul>';
                layer.bindPopup(txt,{autoPan:true});
            }
        });
        gpxlayers[n].addTo(map);
    }

    var coord_min = results[0].features[0].geometry.coordinates[0];
    var coord_max = results[0].features[results[0].features.length-1].
                    geometry.coordinates[0];

    var bounds1 = gpxlayers[0].getBounds();
    var bounds2 = bounds1.extend(gpxlayers[1].getBounds())
    map.fitBounds(bounds2);
}


// get a feature color considering the track pair
// currently under comparison and the used criteria
function getColor(name1, name2, criteria, props){
    var color = 'blue';
    if (criteria === 'distance'){
        if ( ('shorterThan' in props) &&
                (props['shorterThan'].indexOf(name1) !== -1 ||
                 props['shorterThan'].indexOf(name2) !== -1)){
            color = 'green';
        }
        if ( ('longerThan' in props) &&
                (props['longerThan'].indexOf(name1) !== -1 ||
                 props['longerThan'].indexOf(name2) !== -1)){
            color = 'red';
        }
    }
    else if (criteria === 'time'){
        //console.log(props['quickerThan'] + ' // '+name1+ ' // '+name2);
        if ( ('quickerThan' in props) &&
                (props['quickerThan'].indexOf(name1) !== -1 ||
                 props['quickerThan'].indexOf(name2) !== -1)){
            color = 'green';
        }
        if ( ('slowerThan' in props) &&
                (props['slowerThan'].indexOf(name1) !== -1 ||
                 props['slowerThan'].indexOf(name2) !== -1)){
            color = 'red';
        }
    }
    else if (criteria === 'cumulative elevation gain'){
        if ( ('lessPositiveDenivThan' in props) &&
                (props['lessPositiveDenivThan'].indexOf(name1) !== -1 ||
                 props['lessPositiveDenivThan'].indexOf(name2) !== -1)){
            color = 'green';
        }
        if ( ('morePositiveDenivThan' in props) &&
                (props['morePositiveDenivThan'].indexOf(name1) !== -1 ||
                 props['morePositiveDenivThan'].indexOf(name2) !== -1)){
            color = 'red';
        }
    }
    return color;
}

